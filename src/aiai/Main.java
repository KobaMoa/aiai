package aiai;

/**童謡アイアイを表示する
 * @author 200217AM
 *
 */
public class Main {

	/**アイアイを表示する
	 * @return
	 */
	public static String sayAiai(){
		return"アイアイ(アイアイ)";
	}

	/**同じメッセージを複数回表示
	 * @param msg
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg,int count){
		String repeatMsg="";
		for(int i=0; i<count;i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**改行する
	 * @return
	 */
	public static String newLine(){
		return "\n";
	}

	/**おさるさんだよを表示
	 * @return
	 */
	public static String sayFirstMonkey(){
		return "おさるさんだよ";
	}

	/**おさるさんだねを表示
	 * @return
	 */
	public static String saySecondMonkey(){
		return "おさるさんだね";
	}

	/**みなみのしまのを表示
	 * @return
	 */
	public static String saySouth(){
		return "みなみのしまの";
	}

	/**しっぽのながいを表示
	 * @return
	 */
	public static String sayLongTail(){
		return "しっぽのながい";
	}

	/**きのはのおうちを表示
	 * @return
	 */
	public static String sayLeafHouse(){
		return  "きのはのおうち";
	}

	/**おめめのまるいを表示
	 * @return
	 */
	public static String sayRoundEyes(){
		return "おめめのまるい";
	}

	/**アイアイの1番を歌う処理
	 * @param args
	 */
	public static void executeFirstAiai(){
		String lyric="";
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=sayFirstMonkey();
		lyric+=newLine();
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=saySouth();
		lyric+=newLine();
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=sayLongTail();
		lyric+=newLine();
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=sayFirstMonkey();
		System.out.println(lyric);
	}

	/**アイアイの2番を歌う処理
	 *
	 */
	public static void executeSecondAiai(){
		String lyric="";
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=saySecondMonkey();
		lyric+=newLine();
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=sayLeafHouse();
		lyric+=newLine();
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=sayRoundEyes();
		lyric+=newLine();
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=saySecondMonkey();
		System.out.println(lyric);
	}

	public static void main(String[] args) {
		executeFirstAiai();
		executeSecondAiai();
	}

}
